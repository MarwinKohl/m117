## Zusammenfassung Modul 117 - Netzwerkadressierung
Modul 117 behandelt das Thema Netzwerkadressierung. Es werden verschiedene Aspekte der Adressierung im Netzwerk betrachtet. Hier ist eine Zusammenfassung der behandelten Themen:

Zugriffsverfahren und Bustopologie: Eine kurze Einführung in das CSMA/CD-Zugriffsverfahren in einer Bustopologie.

Adressierungskomponenten:

Hostname: Eindeutige Benennung eines Hosts.
MAC-Adresse: Physikalische Adresse des Netzwerkadapters.
IPv4-Adresse: Logische Adresse des Geräts im Netzwerk.
Subnetting:

Aufteilung von IP-Adressen in kleinere Subnetze.
Subnetzmaske und CIDR-Schreibweise.
Netzwerkadresse und Broadcastadresse:

Reservierte Adressen im Subnetz.
Definition und Bedeutung der Netzwerkadresse und Broadcastadresse.
Subnetz-Fallbeispiele:

Praktische Beispiele zur Bestimmung von Netzwerk- und Broadcastadressen sowie Anzahl der IPs und Hosts in einem Subnetz.
Verbindung von Subnetzen:

Notwendigkeit eines Routers zur Kommunikation zwischen Hosts in verschiedenen Subnetzen.
Eintragung des Standardgateways am PC.
Reservierte IPv4-Adressbereiche:

Übersicht über verschiedene für spezielle Zwecke reservierte Adressbereiche.
Loopbackadresse:

Zweck und Bedeutung der Loopbackadresse.
APIPA-Adressen (Zero-Conf):

Zweck und Bedeutung der automatischen privaten IP-Adressierung.
Übersicht über die Aufteilung in Adressbereiche (IPv4).

Aufgabenteil:

Praktische Aufgaben zur Bestimmung von Netzwerk- und Host-IDs, Netzwerk- und Broadcastadressen sowie Bewertung von IP-Adressen und Kommunikationsmöglichkeiten.
Das Modul vermittelt grundlegende Kenntnisse zur Netzwerkadressierung und bietet praktische Übungen zur Anwendung dieser Konzepte.