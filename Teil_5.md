## Modul 117
ARJ Seite 1/1 Jun-23
Der Praxisteil der Netzwerkadressierung beinhaltet die Erstellung und Untersuchung von Netzwerken in CISCO Packet-Tracer. Es sollen die tatsächlichen IP-Adressen und die Erreichbarkeit der verschiedenen Geräte überprüft werden. Wichtige Befehle sind "ping" und "ipconfig".

## Folgende CISCO-Komponenten sollen verwendet werden:

PC (Normaler Desktop-PC)
Switch: 2960
Einfacher Router: 4331
Router für Standortverbindung: PT-Router
DHCP-Server: Server mit DHCP-Dienst
Die Wahl geeigneter IP-Adressen aus dem privaten IP-Bereich obliegt den Teilnehmern. Zuerst sollte ein logisches Topo mit allen Namen und Adressangaben erstellt werden (IPERKA). Es ist wichtig zu überprüfen, ob die Netzwerkschnittstellen der Geräte aktiv sind und ob die Standardgateway-Einstellungen (Default-Router) korrekt sind.

## Die Aufgaben lauten wie folgt:

**Zwei PCs sind an Switch-1 angeschlossen, während zwei weitere PCs an Switch-2 angeschlossen sind. Switch-1 und Switch-2 sind über ein Ethernetkabel verbunden. Es soll eine statische IP-Adressierung verwendet werden, wobei alle PCs sich im selben Subnetz befinden.**

In dieser Aufgabe sind zwei PCs an Switch-1 angeschlossen und zwei weitere PCs an Switch-2. Die beiden Switches sind über ein Ethernetkabel miteinander verbunden. Es wird eine statische IP-Adressierung verwendet, was bedeutet, dass die IP-Adressen manuell auf jedem PC konfiguriert werden. Da alle PCs im selben Subnetz sind, können sie direkt miteinander kommunizieren, ohne dass ein Router benötigt wird

**Ähnlich wie Aufgabe 1, jedoch werden die IP-Adressen dynamisch über einen DHCP-Server zugewiesen. Der DHCP-Server ist mit Switch-2 verbunden.**
In dieser Aufgabe bleibt die Konfiguration gleich wie in Aufgabe 1, mit dem Unterschied, dass die IP-Adressen jetzt dynamisch über einen DHCP-Server zugewiesen werden. Der DHCP-Server ist an Switch-2 angeschlossen. Die PCs senden DHCP-Anfragen an den Server, der ihnen automatisch IP-Adressen zuweist. Dadurch entfällt die manuelle Konfiguration der IP-Adressen auf den PCs.

**Ähnlich wie Aufgabe 2, jedoch kann der DHCP-Server nicht erreicht werden, da er offline, ausgeschaltet oder defekt ist.**
In dieser Aufgabe bleibt die Konfiguration wie in Aufgabe 2, aber der DHCP-Server ist nicht erreichbar, entweder weil er offline ist, ausgeschaltet oder defekt. In diesem Fall können die PCs keine IP-Adressen vom DHCP-Server beziehen und müssen entweder auf vorher zugewiesene statische IP-Adressen zurückgreifen oder es kann keine Netzwerkverbindung hergestellt werden.



**Zwei PCs sind in Subnetz-1 an Switch-1 angeschlossen, während zwei weitere PCs in Subnetz-2 an Switch-2 angeschlossen sind. Switch-1 und Switch-2 sind über ein Ethernetkabel verbunden. Es soll eine statische IP-Adressierung verwendet werden.**
Hier sind zwei PCs im Subnetz-1 an Switch-1 angeschlossen und zwei weitere PCs im Subnetz-2 an Switch-2. Die beiden Switches sind über ein Ethernetkabel miteinander verbunden. Jedes Subnetz hat seine eigene statische IP-Adressierung. Die PCs können nur innerhalb ihres jeweiligen Subnetzes kommunizieren, da sie über separate IP-Adressbereiche verfügen.

**Ähnlich wie Aufgabe 4, jedoch sind die beiden Switches nicht direkt miteinander verbunden, sondern über einen Router. Es soll eine statische IP-Adressierung verwendet werden.**
In dieser Aufgabe bleiben die PCs und Switches wie in Aufgabe 4, aber die beiden Switches sind jetzt über einen Router verbunden. Die beiden Switches sind mit unterschiedlichen Subnetzen konfiguriert, und der Router verbindet diese Subnetze miteinander. Statische IP-Adressierung wird verwendet. Der Router fungiert als Vermittler und ermöglicht den Datenverkehr zwischen den Subnetzen.

**Fortgeschrittenen-Aufgabe: Es sollen zwei PCs in Subnetz-A, zwei PCs in Subnetz-B und zwei PCs in Subnetz-C erstellt werden. Subnetz-A und Subnetz-B werden über Router-X verbunden, während Subnetz-C mit Router-Y verbunden ist. Router-X und Router-Y sind über eine serielle Leitung (Telefon) verbunden und erzeugen das Subnetz-D. Es soll eine statische IP-Adressierung mit privaten IP-Adressen verwendet werden. Hierbei handelt es sich um eine sogenannte Standortverbindung über eine Zweidrahtstandleitung wie Telefon. Die Routingtabellen der beiden Router müssen ergänzt werden.**
In dieser fortgeschrittenen Aufgabe gibt es drei Subnetze (A, B, C) mit jeweils zwei PCs. Subnetz-A und Subnetz-B sind über Router-X verbunden, während Subnetz-C über Router-Y verbunden ist. Router-X und Router-Y sind über eine serielle Leitung (Telefon) miteinander verbunden, wodurch ein viertes Subnetz, Subnetz-D, entsteht. Die IP-Adressierung erfolgt statisch, und es werden private IP-Adressen verwendet. Durch die Konfiguration der Routingtabellen in den Routern können die PCs in den verschiedenen Subnetzen miteinander kommunizieren, indem sie den richtigen Router als Gateway verwenden.

Die Lösungen sollen zusammen mit beschrifteten Bildern (Packet-Tracer-Printscreens) im ePortfolio abgegeben werden.