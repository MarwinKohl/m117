# Reflexionen

**Tag 1 (26.05.2023):**
Heute habe ich einen guten Einstieg in das Modul M117 erhalten, was sich um Netzinfrastrukur handelt. Heute besprachen wir, wie ein simples Netzwerk aufgebaut ist und untersuchten später mit einem Auftrag, wie das Heutige WLAN funktioniert.

![Alt text](images/Screenshot%202023-05-26%20162206.png)

Weiter durften wir uns über die Topologien schlau machen, indem wir die Vor- und Nachteile der Bus-, Ring-, Stern-, Baum- oder Meshtopologien vergleichen. Hierblei unterscheiden die sich in verschiedensten Arten.

Bei einem weiteren Input haben wir auch die Vor und Nachteile angeschaut. Nun von den Verkabelungen.

**Tag 2 (2.06.2023):**
Beim virtuellen Zugang zum Internet wird eine Verbindung über ein virtuelles Netzwerk hergestellt, das über das WAN oder Core-Network zugänglich ist. Als virtueller Router im LAN ermögliche ich die Verbindung von IP-Netzwerken und biete Funktionen wie DHCP, DNS, NAT, Firewall, WLAN-Access-Point sowie die Integration von Druckern und Schnurlostelefonen. Bei der Bewertung von ISP-Angeboten sollten Technologie, Leistung, Kosten, virtuelle Router, Gebühren, Support, Vertragsdetails, Netzverfügbarkeit und Skalierbarkeit berücksichtigt werden.

Ich habe heute viel gelernt und es hat mir besonders viel Spass gemacht.

![Netzwerk bild](https://www.tornadosoft.de/magazin/wp-content/uploads/2017/03/Netzwerk-einrichten.jpg)

**Tag 3 (9.06.2023):**

Ich habe meine virtuellen Aktivitäten genutzt, um meine Kreativität zu entfalten, mich mit Freunden zu vernetzen und neue Fähigkeiten zu erlernen. Von der Teilnahme an virtuellen Kunstworkshops und Musikstunden über das Spielen von Online-Spielen mit meinen Freunden bis hin zur Verbesserung meiner Kochkünste mit Hilfe von Video-Tutorials - ich habe das Beste aus der virtuellen Welt gemacht, um meine Interessen zu verfolgen und mich weiterzuentwickeln.

Heute war ein lehrreicher Tag und ich habe dabei viel Spass gehabt.

![Netzwerk bild](https://i.pinimg.com/originals/6c/8f/01/6c8f01e16ecc5e19d817902ae3053ed0.png)

**Tag 4 (16.06.2023):**

Ich habe alle Netzwerkkonfigurationen und -tests virtuell mit Cisco Packet-Tracer durchgeführt. Dabei habe ich verschiedene Cisco-Komponenten wie PCs, Switches, Router und einen DHCP-Server verwendet. In den Aufgaben ging es darum, Netzwerke zu erstellen, IP-Adressen zuzuweisen, die Erreichbarkeit der Geräte zu überprüfen und Routingtabellen anzupassen. Von einfachen Subnetzkonfigurationen bis hin zu komplexen Standortverbindungen mit serieller Leitung habe ich verschiedene Szenarien durchgespielt.

Es war ein lehrreicher Tag, an dem ich mich besonders gut unterhalten habe.

![Netzwerk bild](https://www.truegossiper.com/wp-content/uploads/2019/11/5-9.jpg)

**Tag 5 (23.06.2023):**

Ich habe virtuell Daten zwischen verschiedenen Subnetzen übertragen. Dabei habe ich IP-Adressen verwendet, um die Zielgeräte zu identifizieren, und MAC-Adressen des Routers genutzt, um den Datenverkehr zwischen den Subnetzen zu vermitteln. Falls keine Verbindung zu einem DHCP-Server hergestellt werden konnte, habe ich automatisch generierte APIPA-Adressen verwendet, um vorübergehend Netzwerkzugriff zu ermöglichen.

Mir hat es heute grossen Spass gemacht, viel Neues zu lernen.

![Netzwerk bild](https://www.loewenstark.com/wp-content/uploads/IP-Adressenaufbau.jpg)

**Tag 6 (30.06.2023):**

Heute habe ich viel über virtuelle Maschinen gelernt. Dabei habe ich erfahren, dass virtuelle Maschinen eine Möglichkeit bieten, Gastbetriebssysteme innerhalb einer sicheren Umgebung auf einem Hostsystem auszuführen. Es war interessant zu sehen, wie virtuelle Netzwerkkarten, virtuelle Festplatten und virtuelle Bildschirme eingesetzt werden, um die virtuellen Maschinen funktionsfähig zu machen. Zudem habe ich erkannt, dass virtuelle Maschinen auf physikalisches RAM zugreifen und sogar ein eigenes BIOS haben. Alles in allem war es ein spannender Einblick in die Welt der virtuellen Maschinen und ihrer Komponenten.

![Netzwerk bild](https://i.ytimg.com/vi/EZr0Hv3fn00/maxresdefault.jpg)


**Tag 7 (30.06.2023)**


Heute habe ich mich mit den Benutzerrechten und Freigaben verschiedener User beschäftigt. Es war interessant zu erfahren, wie diese Mechanismen dazu dienen, den Zugriff auf Ressourcen in einem Computersystem zu steuern. Die Benutzerrechte ermöglichen es, bestimmte Aktionen auszuführen oder auf bestimmte Dateien und Ordner zuzugreifen, während Freigaben den Austausch von Daten und Ressourcen zwischen Benutzern ermöglichen. Ich habe gelernt, wie wichtig es ist, die Benutzerrechte und Freigaben sorgfältig zu verwalten, um die Sicherheit und den Schutz sensibler Informationen zu gewährleisten.

![Netzwerk bild](https://manula.s3.amazonaws.com/user/3511/img/benutzereinstellungen2-de.png)