## Zusammenfassung Modul 117 - Netzwerkadressierung mit Cisco Packet-Tracer

Modul 117 beinhaltet eine praktische Übung zur Netzwerkadressierung mit Hilfe von Cisco Packet-Tracer. Es werden verschiedene Netzwerke erstellt und untersucht. Folgende Cisco-Komponenten kommen dabei zum Einsatz:

PC (Normaler Desktop-PC)
Switch: 2960
Einfacher Router: 4331
Router für Standortverbindung: PT-Router
DHCP-Server: Server mit DHCP-Dienst
Es gibt insgesamt sechs Aufgaben, die zu bearbeiten sind:

## Aufgabe 1: Statische IP-Adressierung im selben Subnetz
Zwei PCs sind an Switch-1 angeschlossen und zwei weitere PCs an Switch-2. Switch-1 und Switch-2 sind über ein Ethernetkabel miteinander verbunden. Es wird eine statische IP-Adressierung verwendet, und alle PCs befinden sich im selben Subnetz.

## Aufgabe 2: Dynamische IP-Adressierung über DHCP-Server
Diese Aufgabe ist ähnlich wie Aufgabe 1, jedoch werden die IP-Adressen der PCs dynamisch über einen DHCP-Server zugewiesen. Der DHCP-Server ist an Switch-2 angeschlossen.

## Aufgabe 3: DHCP-Server nicht erreichbar
Das Szenario gleicht Aufgabe 2, jedoch wird angenommen, dass der DHCP-Server nicht erreichbar ist (z. B. offline, ausgeschaltet oder defekt).

## Aufgabe 4: Statische IP-Adressierung in unterschiedlichen Subnetzen
Zwei PCs befinden sich im Subnetz-1 und sind an Switch-1 angeschlossen, während zwei weitere PCs im Subnetz-2 an Switch-2 angeschlossen sind. Die beiden Switches sind über ein Ethernetkabel verbunden. Es wird eine statische IP-Adressierung verwendet.

## Aufgabe 5: Statische IP-Adressierung mit Routerverbindung
Die Konfiguration ähnelt Aufgabe 4, jedoch sind die beiden Switches nicht direkt miteinander verbunden. Stattdessen erfolgt die Verbindung über einen Router. Es wird weiterhin eine statische IP-Adressierung verwendet.

## Aufgabe 6: Fortgeschrittene Standortverbindung
Diese Aufgabe erfordert fortgeschrittene Kenntnisse. Es gibt drei Subnetze (A, B und C) mit jeweils zwei PCs. Subnetz-A ist über Router-X mit Subnetz-B verbunden, während Subnetz-C über Router-Y verbunden ist. Router-X und Router-Y sind über eine serielle Leitung (Telefonleitung) miteinander verbunden, was Subnetz-D ergibt. Es wird eine statische IP-Adressierung mit privaten IP-Adressen verwendet.

Die Lösungen für alle Aufgaben sollten als beschriftete Bilder (Packet-Tracer-Printscreens) im ePortfolio abgegeben werden.