## Teil 1: Netzwerkverkabelung
Einfach/Unidirektional: Datenübertragung erfolgt nur in eine Richtung.

- Halb-Duplex/Bidirektional: Datenübertragung in beide Richtungen ist möglich, aber nicht gleichzeitig.

- Duplex/Bidirektional: Datenübertragung in beide Richtungen ist gleichzeitig möglich.

- In heutigen LANs wird in der Regel der Duplex-Modus verwendet, da die meisten Netzwerke Vollduplex-Ethernet nutzen. Dadurch können Daten gleichzeitig in beide Richtungen übertragen werden, was die Effizienz und Leistung verbessert.


## Netzwerktopologien

- Bei der Bus-Topologie sind alle Geräte über ein gemeinsames Kommunikationskabel verbunden.
- In der Ring-Topologie bilden die Geräte einen geschlossenen Ring.
- Die Stern-Topologie verbindet alle Geräte mit einem zentralen Knotenpunkt (Switch/Hub).
- Die Baum-Topologie besteht aus einer Hierarchie von Switches, die die Geräte über mehrere Ebenen miteinander verbinden.

In der Mesh-Topologie ist jedes Gerät direkt mit jedem anderen Gerät verbunden.
Diese Topologien unterscheiden sich in ihrer Struktur, Ausfallsicherheit, Skalierbarkeit und Komplexität. Jede Topologie hat ihre Vor- und Nachteile. Zum Beispiel bietet die Bus-Topologie eine einfache Installation, ist aber anfällig für Ausfälle an einer einzigen Stelle. Die Stern-Topologie hingegen bietet eine hohe Ausfallsicherheit, erfordert jedoch mehr Verkabelungsaufwand.

Der spezielle Fall Nr. 5 bezieht sich auf die Punkt-zu-Punkt-Verbindung in der Mesh-Topologie, bei der jedes Gerät direkt mit jedem anderen Gerät verbunden ist.
## Draht, Litze und Glas
- Der Draht besteht aus einfachen Kupferdrähten, die in Ethernetkabeln verwendet werden.
- Die Litze besteht aus mehreren dünnen Kupferdrähten, die zu einem Kabelstrang verdrillt sind. Sie bietet Flexibilität und eignet sich gut für Patchkabel.
- Das Glas besteht aus optischen Fasern, die Daten mittels Lichtsignalen übertragen.
Die drei Materialien haben jeweils ihre Vor- und Nachteile:

Draht ist einfach und kostengünstig, aber anfällig für elektromagnetische Störungen.
Litze ist flexibel und gut geeignet für Patchkabel, aber anfälliger für Beschädigungen.
Glas bietet eine hohe Bandbreite und ist immun gegen elektromagnetische Störungen, ist jedoch teurer und schwieriger zu installieren.
In der Netzwerktechnik werden Draht und Litze in Kupfer-Ethernetkabeln verwendet, während Glasfasern in Glasfaserkabeln eingesetzt werden.





## Störeinflüsse abwehren

Die Kommunikation kann durch verschiedene Störungen beeinträchtigt werden. Hierzu zählen:

- Elektromagnetische Interferenzen (EMI)
- Radiofrequenzstörungen (RFI)
- Übersprechen
- Impulsrauschen
- Signalverlust
Um diese Störungen zu minimieren, können verschiedene Maßnahmen ergriffen werden. Dazu gehören die Schirmung der Kabel, die Verwendung von geschirmten Steckern, die Vermeidung von Kabelinterferenzen sowie der Einsatz von Signalverstärkern. Diese Maßnahmen dienen dazu, die Auswirkungen der Störungen zu reduzieren und eine zuverlässigere Kommunikation zu gewährleisten.

## Kabel-Abschirmung


Die Bezeichnung eines Kabels gibt Informationen über den inneren Aufbau des Kabels:

- Unshielded [U]: Das Kabel besitzt keine Abschirmung.
- Screened [S]: Das Kabel ist mit einem Kupfergeflecht über das gesamte Kabel gegen niederfrequente Störungen abgeschirmt.
- Shielded [S]: Das Kabel ist mit einem Kupfergeflecht über die verdrillten Aderpaare gegen niederfrequente Störungen abgeschirmt.
- Foiled [F]: Das Kabel ist mit einer Folienabschirmung gegen hochfrequente Störungen entweder über das gesamte Kabel oder über die Aderpaare abgeschirmt.
- Twisted Pair [TP]: Das Kabel besteht aus verdrillten Aderpaaren, um Gleichtaktstörungen zu unterdrücken.
Die Bezeichnungen der Kabelquerschnitte in den Skizzen können ohne weitere Informationen nicht direkt abgelesen werden.

## Kabelkategorien
Ethernetkabel werden in Kabelkategorien eingeteilt, wobei höhere Kategorien einen höheren maximalen Datendurchsatz bieten. Der Kabelaufbau kann sich dabei unterscheiden.
Es fehlen Informationen zu den Kabelkategorien.
## Ethernet-Medientypen
Hier sind einige Beispiele für verschiedene Ethernet-Medientypen:

1000Base-T: Diese Variante verwendet eine Kupferverbindung und folgt dem Stern/Duplex-Verbindungsmuster. Sie entspricht der IEEE-Norm 802.3ab und bietet einen Brutto-Datendurchsatz von 1 Gb/s. Die minimale erforderliche Kabelkategorie beträgt CAT5e, und die maximale Segmentlänge beträgt 100 Meter.

1000Base-SX: Diese Variante nutzt Glasfaser/Fibre als Übertragungsmedium und folgt ebenfalls dem Stern/Duplex-Muster. Sie entspricht der IEEE-Norm 802.3z und bietet einen Brutto-Datendurchsatz von 1 Gb/s. Die Technologie, die bei dieser Variante verwendet wird, ist Short-Wavelength (850nm). Die maximale Segmentlänge beträgt 275 Meter für 62.5μm Multimode und 550 Meter für 50μm Multimode.

Bitte beachten Sie, dass es noch weitere Ethernet-Medientypen gibt, die hier nicht aufgeführt sind.

## Universelle Gebäudeverkablung (UGV)

Logische Topologie: Dies umfasst die Darstellung der Verbindungen zwischen verschiedenen Komponenten wie PCs, Servern, Switches und Routern. Hierbei werden keine Details zu RJ45-Steckdosen und Verkabelungstechnologien berücksichtigt.

Physische Topologie oder Verkabelungsplan: Dieser zeigt die Verkabelung anhand eines Gebäudegrundrisses. Er dient als Leitfaden für den Installateur, um die tatsächliche Verkabelung vorzunehmen.

Es ist wichtig, dass das logische Layout und der Verkabelungsplan hinsichtlich der Anschlüsse und Beschriftungen der Komponenten übereinstimmen.

Bei Festinstallationen wird die universelle Gebäudeverkablung (UGV) verwendet. Dabei werden die Komponenten von einer Netzwerksteckdose zur nächsten mit Patchkabeln verbunden.